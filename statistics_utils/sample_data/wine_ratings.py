import pandas as pd

wine_ratings = pd.DataFrame(
    data=[["A", "A", "A", 2, 5, 7, 6, 3, 6, 7],
          ["A", "A", "A", 4, 4, 4, 2, 4, 4, 3],
          ["B", "A", "B", 5, 2, 1, 1, 7, 1, 1],
          ["B", "A", "B", 7, 2, 1, 2, 2, 2, 2],
          ["B", "B", "B", 3, 5, 6, 5, 2, 6, 6],
          ["B", "B", "A", 3, 5, 4, 5, 1, 7, 5]],
    columns=["E1 fruity", "E1 woody", "E1 coffee",
             "E2 red fruit", "E2 roasted", "E2 vanillin", "E2 woody",
             "E3 fruity", "E3 butter", "E3 woody"],
    index=["Wine {}".format(i + 1) for i in range(6)])

if __name__ == "__main__":
    print(wine_ratings)
