from typing import Tuple, Dict, List

import numpy as np
import pandas as pd


def generate_y_category(category_means: Dict[int, float], n_obs: int = 1000, sd: float = 1.,
                        category_probas: List[float] = None) -> Tuple[pd.Series, pd.Series]:
    n_cat = len(category_means)
    if category_probas is None:
        category_probas = [1 / n_cat] * n_cat

    if sum(category_probas) != 1.0:
        raise AssertionError(f"Category probabilities sum up to {sum(category_probas)} != 1.")

    category_vector = pd.Series(np.random.choice(list(category_means.keys()), n_obs, p=category_probas), name="cat")
    mu = np.array([category_means[i] for i in category_vector])
    eps = np.random.randn(n_obs) * sd
    y = mu + eps
    y = pd.Series(y, name="y")
    return y, category_vector


def generate_x_norm(n_x: int, n_obs: int = 1000, mus: List[float] = None, sds: List[float] = None) -> pd.DataFrame:
    if mus is None:
        mus = [0] * n_x

    if sds is None:
        sds = [1] * n_x

    x = np.array([mus[x_index] + np.random.randn(n_obs) * sds[x_index]
                  for x_index in range(n_x)]).reshape(n_x, n_obs).T
    x_df = pd.DataFrame(x)
    x_df.columns = [f"x_{x_index}" for x_index in range(n_x)]
    return x_df


def generate_y_x(beta: List[float] = None, n_obs: int = 1000, sd: float = 1.) -> pd.DataFrame:
    if beta is None:
        beta = [1., 1.]
    x_df = generate_x_norm(n_x=len(beta) - 1, n_obs=n_obs)
    x = np.array(x_df).reshape(n_obs, -1)
    mu = beta[0] + x @ np.array(beta[1:]).reshape(-1, 1)
    eps = (np.random.randn(n_obs) * sd).reshape(-1, 1)
    y = mu + eps

    y_df = pd.DataFrame(y)
    y_df.columns = ["y"]

    data = pd.concat([y_df, x_df], axis=1)
    return data


if __name__ == "__main__":
    data = generate_y_x(beta=[1.0, 2.0, 3.0])
    print(data)
