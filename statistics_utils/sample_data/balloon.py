import pandas as pd


def get_balloon_data() -> pd.DataFrame:
    color = ["YELLOW", "YELLOW", "YELLOW", "YELLOW", "YELLOW", "YELLOW", "YELLOW", "YELLOW", "YELLOW", "PURPLE",
             "PURPLE", "PURPLE", "PURPLE", "PURPLE", "PURPLE", "PURPLE", "PURPLE", "PURPLE", "PURPLE"]
    size = ["SMALL", "SMALL", "SMALL", "SMALL", "LARGE", "LARGE", "LARGE", "LARGE", "LARGE", "SMALL", "SMALL", "SMALL",
            "SMALL", "SMALL", "LARGE", "LARGE", "LARGE", "LARGE", "LARGE"]
    action = ["STRETCH", "STRETCH", "DIP", "DIP", "STRETCH", "STRETCH", "STRETCH", "DIP", "DIP", "STRETCH", "STRETCH",
              "STRETCH", "DIP", "DIP", "STRETCH", "STRETCH", "STRETCH", "DIP", "DIP"]
    age = ["ADULT", "CHILD", "ADULT", "CHILD", "ADULT", "ADULT", "CHILD", "ADULT", "CHILD", "ADULT", "ADULT", "CHILD",
           "ADULT", "CHILD", "ADULT", "ADULT", "CHILD", "ADULT", "CHILD"]
    inflated = ["T", "F", "F", "F", "T", "T", "F", "F", "F", "T", "T", "F", "F", "F", "T", "T", "F", "F", "F"]
    dataframe = pd.DataFrame({"Color": color, "Size": size, "Action": action, "Age": age, "Inflated": inflated})
    return dataframe
