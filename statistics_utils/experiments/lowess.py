import matplotlib.pyplot as plt
import numpy as np
from statsmodels.nonparametric.smoothers_lowess import lowess

n_obs = 1000
x = np.random.uniform(low=-3, high=3, size=n_obs)
eps = np.random.randn(n_obs) * .2
y = np.sin(x) + eps

plt.scatter(x, y, alpha=0.5)
plt.scatter(x, np.sin(x), label="true function")

lowess_curve = lowess(y, x)
plt.plot(*lowess_curve.T, color="black", label="LOWESS")

plt.legend()
plt.show()
