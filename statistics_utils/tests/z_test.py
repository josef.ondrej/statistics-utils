from typing import Dict, Tuple, List

import scipy.stats

from statistics_utils.utils.string_utils import prettify_cathegory_name

_mean = "mean"
_std_err = "std_err"
_z = "z"
_p_val = "p_val"
_ci_lower = "ci_lower"
_ci_upper = "ci_upper"
_alpha = "alpha"
_use_t_distribution = "use_t_distribution"

_z_test_keys = [_mean, _std_err, _z, _p_val, _ci_lower, _ci_upper, _alpha]


def unpack_z_test_result(z_test_result: Dict[str, float]) -> Tuple[float]:
    mu, std_err, z, p_val, ci_low, ci_up, alpha = [z_test_result[key] for key in _z_test_keys]
    return mu, std_err, z, p_val, ci_low, ci_up, alpha


def z_test(mean: float, std_err: float, alpha: float = .05, use_t_distribution: bool = False) -> Dict[str, float]:
    z = mean / std_err

    distribution = scipy.stats.norm()

    if use_t_distribution:
        distribution = scipy.stats.t(df=1)

    p_val = 2 * (1 - distribution.cdf(abs(z)))
    delta = distribution.ppf(1 - alpha / 2) * std_err

    ci_lower, ci_upper = mean - delta, mean + delta
    t_test_result = {_mean: mean,
                     _std_err: std_err,
                     _z: z,
                     _p_val: p_val,
                     _ci_lower: ci_lower,
                     _ci_upper: ci_upper,
                     _alpha: alpha,
                     _use_t_distribution: False}

    return t_test_result


def z_test_result_to_str(z_test_result: Dict[str, float], name: str = None) -> Tuple[str, str]:
    mu, std_err, z, p_val, ci_low, ci_up, alpha = unpack_z_test_result(z_test_result)
    if name is None:
        name = " "
    name_short = prettify_cathegory_name(name, 15)
    header = f"{' ' * 26}coef       std err                z           P>|z|          [{alpha / 2:.3f}            {1 - alpha / 2:.3f}]"
    test_summary = f"{name_short}  {mu:>12.4f}   {std_err:>12.3f}     {z:>12.3f}   {p_val:>12.3f}     {ci_low:>12.3f}     {ci_up:>12.3f}"
    return header, test_summary


def z_test_results_to_str(z_test_results: List[Dict[str, float]], names: List[str] = None) -> str:
    if names is None:
        names = [None] * len(z_test_results)

    test_summaries = []
    header = None

    for z_test_result, name in zip(z_test_results, names):
        header, test_summary = z_test_result_to_str(z_test_result, name)
        test_summaries.append(test_summary)
    line_len = 112
    final_summary = "=" * line_len + "\n"
    final_summary += header + "\n"
    final_summary += "-" * line_len + "\n"
    for test_summary in test_summaries:
        final_summary += test_summary + "\n"
    final_summary += "=" * line_len

    return final_summary


if __name__ == "__main__":
    mean = 1.251792
    std_err = 0.03773145
    t_test_result = z_test(mean, std_err)
    print(t_test_result)

    # ==============================================================================
    #                  coef    std err          z      P>|z|      [0.025      0.975]
    # ------------------------------------------------------------------------------
    # cat_1          1.2518      0.038     33.176      0.000       1.178       1.326

    z_test_result_1 = z_test(1000.56, 1.22)
    z_test_result_2 = z_test(0.41, 0.22)
    z_test_result_3 = z_test(12.3, 1.5)

    z_test_results = [z_test_result_1, z_test_result_2, z_test_result_3]
    names = ["var1", "var2", "var3"]

    summary = z_test_results_to_str(z_test_results, names)
    print(summary)
