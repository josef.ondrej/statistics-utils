from typing import Dict

import scipy.stats
from statsmodels.genmod.families import Gaussian
from statsmodels.genmod.families.tests.test_link import identity
from statsmodels.genmod.generalized_linear_model import GLMResults, GLM

from statistics_utils.sample_data.generate_regression_data import generate_y_x
from statistics_utils.utils.string_utils import get_significance_stars

_ll_model = "log_likelihood_model"
_ll_null = "log_likelihood_baseline_model"
_df_model = "df_model"  # Degrees of freedom - 1
_df_baseline = "df_baseline_model"  # Degrees of freedom - 1
_chi2 = "chi2"
_p_val = "p_val"
_model_result = "model_result"


def likelihood_ratio_test_glm_model(model_result: GLMResults, baseline_model_df: int = 0,
                                    baseline_model_loglik: float = None):
    if baseline_model_loglik is None:
        baseline_model_loglik = model_result.llnull

    df_diff = model_result.df_model - baseline_model_df

    chi2_statistic = 2 * (model_result.llf - baseline_model_loglik)
    p_val = 1 - scipy.stats.chi2.cdf(x=chi2_statistic, df=df_diff)

    likelihood_ratio_test_glm_model = {
        _ll_model: model_result.llf,
        _ll_null: baseline_model_loglik,
        _df_model: model_result.df_model,
        _df_baseline: baseline_model_df,
        _chi2: chi2_statistic,
        _p_val: p_val,
        _model_result: model_result
    }

    return likelihood_ratio_test_glm_model


def LR_test_result_to_str(LR_test_result: Dict[str, float]) -> str:
    signif_stars = get_significance_stars(LR_test_result[_p_val])
    resul_str_repr = f"Model log-lik:                {LR_test_result[_ll_model]:>15.4f} (df = {LR_test_result[_df_model] + 1}) \n"
    resul_str_repr += f"Baseline model log-lik:       {LR_test_result[_ll_null]:>15.4f} (df = {LR_test_result[_df_baseline] + 1}) \n"
    resul_str_repr += f"Chi2 statistic:               {LR_test_result[_chi2]:>15.4f} \n"
    resul_str_repr += f"P(chi2(df={LR_test_result[_df_model] - LR_test_result[_df_baseline]}) > LR):           {LR_test_result[_p_val]:>15.4f}  {signif_stars}"
    return resul_str_repr


if __name__ == "__main__":
    data = generate_y_x()
    print(data.head())

    print("\n" + "-" * 100 + "\n")

    model = GLM.from_formula("y ~ x_0", data=data, family=Gaussian(link=identity))
    model_result = model.fit()
    LR_test_result = likelihood_ratio_test_glm_model(model_result)
    LR_test_result_repr = LR_test_result_to_str(LR_test_result)
    print(LR_test_result_repr)
