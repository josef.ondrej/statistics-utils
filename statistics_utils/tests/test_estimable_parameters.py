from typing import List, Dict

import numpy as np

from statistics_utils.sample_data.generate_regression_data import generate_y_category
from statistics_utils.tests.z_test import z_test, z_test_results_to_str
from statistics_utils.variable_transformations.to_dummies import get_dummies


def _test_estimable_parameter(beta_estimates: np.array,
                              beta_asymptotic_covariance_matrix: np.array,
                              parameter_beta_coefficients: List[float]) -> Dict[str, float]:
    beta_estimates = np.array(beta_estimates).reshape(-1, 1)
    beta_coefficients = np.array(parameter_beta_coefficients).reshape(-1, 1)
    theta_mean = np.sum(beta_estimates * beta_coefficients)
    theta_std_err = np.sqrt(beta_coefficients.T @ beta_asymptotic_covariance_matrix @ beta_coefficients)

    theta_mean = theta_mean
    theta_std_err = theta_std_err[0][0]
    z_test_result = z_test(theta_mean, theta_std_err)
    return z_test_result


def test_estimable_parameters(beta_estimates: np.array, beta_information_matrix: np.array,
                              estimable_parameter_beta_coefficients: List[List[float]]) \
        -> List[List[Dict[str, float]]]:
    """
    Gives tests for significance of estimable parameters theta that are calculated as a linear combination
    of some parameters beta, for which we know their estimates and inforamtion matrix.
    Useful for testing estimable parameters in regression models for example.
    :param beta_estimates:
    :param beta_information_matrix:
    :param estimable_parameter_beta_coefficients:
    :return:
    """
    beta_asymptotic_covariance_matrix = -np.linalg.inv(beta_information_matrix)
    z_test_results = [_test_estimable_parameter(beta_estimates, beta_asymptotic_covariance_matrix, coeff)
                      for coeff in estimable_parameter_beta_coefficients]

    return z_test_results


if __name__ == "__main__":
    from statsmodels.genmod.generalized_linear_model import GLM
    import pandas as pd
    from statsmodels.genmod.families import Gaussian
    from statsmodels.genmod.families.tests.test_link import identity

    category_means = {1: 1.2, 2: 2.3, 3: 4.4}
    n_obs = 100
    y, cat = generate_y_category(category_means, n_obs)
    dummies, _ = get_dummies(cat)
    data = pd.concat([y, dummies], axis=1)

    formula = "y ~ -1 + cat_1 + cat_2 + cat_3"
    model = GLM.from_formula(formula=formula, data=data, family=Gaussian(link=identity))
    model_result = model.fit()
    print("Model summary for each category: ")
    print(model_result.summary())

    formula = "y ~ cat_1 + cat_2"
    model = GLM.from_formula(formula=formula, data=data, family=Gaussian(link=identity))
    model_result = model.fit()

    beta_estimates = model_result.params
    beta_information_matrix = model.information(model_result.params)

    parameter_beta_coefficients = [[1, 1, 0], [1, 0, 1], [1, 0, 0]]
    parameter_names = ["cat_1", "cat_2", "cat_3"]

    z_test_results = test_estimable_parameters(beta_estimates, beta_information_matrix, parameter_beta_coefficients)
    summary = z_test_results_to_str(z_test_results, parameter_names)
    print(summary)
