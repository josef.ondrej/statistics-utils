import numpy as np
import prince
from sklearn.decomposition import PCA

from statistics_utils.sample_data.generate_regression_data import generate_x_norm

if __name__ == "__main__":
    n_components = 2
    n_iter = 3
    rescale_with_mean = True
    rescale_with_std = True
    copy = True
    check_input = True
    engine = "auto"
    random_state = 42

    n_x = 4
    n_obs = 15

    x = generate_x_norm(n_x=n_x, n_obs=n_obs)
    x = (x - np.mean(x, axis=0)) / np.sqrt(np.var(x, axis=0))

    # Prince
    pca = prince.PCA(n_components=n_components,
                     n_iter=n_iter,
                     rescale_with_mean=rescale_with_mean,
                     rescale_with_std=rescale_with_std,
                     copy=copy,
                     check_input=check_input,
                     engine=engine,
                     random_state=random_state)

    pca.fit(x)

    # sklearn
    pca_sklearn = PCA(n_components=n_components, copy=copy)
    transformed = pca_sklearn.fit_transform(x)

    assert np.isclose(pca.total_inertia_, np.sum(x.to_numpy() ** 2))
    assert np.all(np.isclose(pca.eigenvalues_, pca_sklearn.explained_variance_ * (n_obs - 1)))
    assert np.all(np.isclose(pca.explained_inertia_, pca_sklearn.explained_variance_ratio_))
    assert np.all(np.isclose(pca.transform(x), pca_sklearn.transform(x)))
