import numpy as np
import pandas as pd
import prince
from prince import svd, PCA

from statistics_utils.sample_data.balloon import get_balloon_data

if __name__ == "__main__":
    n_components = 2
    n_iter = 3
    copy = True
    check_input = True
    engine = "auto"
    random_state = 42

    balloons = get_balloon_data()

    # ------------------------------------------------------------------------------------------------------------------
    # CA
    ca = prince.CA(n_components=n_components,
                   n_iter=n_iter,
                   copy=copy,
                   check_input=check_input,
                   engine=engine,
                   random_state=random_state)

    one_hot = pd.get_dummies(balloons)
    ca.fit(one_hot)

    X = one_hot.to_numpy()
    X = X / np.sum(X)
    r = pd.Series(X.sum(axis=1)).to_numpy()
    c = pd.Series(X.sum(axis=0)).to_numpy()
    S = np.diag(r ** -0.5) @ (X - np.outer(r, c)) @ np.diag(c ** -0.5)
    # CA is PCA applied instead of on covariance matrix on the matrix S

    U_, s_, V_ = svd.compute_svd(X=S,
                                 n_components=n_components,
                                 n_iter=n_iter,
                                 random_state=random_state,
                                 engine=engine)

    assert np.all(np.isclose(ca.eigenvalues_, np.square(s_)))

    total_inertia = np.einsum('ij,ji->', S, S.T)
    assert np.isclose(total_inertia, np.sum(S ** 2))
    assert np.isclose(ca.total_inertia_, total_inertia)
    assert np.all(np.isclose(ca.explained_inertia_, np.square(s_) / total_inertia))

    assert np.all(np.isclose(ca.row_masses_, np.sum(X, axis=1)))
    assert np.all(np.isclose(ca.col_masses_, np.sum(X, axis=0)))

    # ------------------------------------------------------------------------------------------------------------------
    # MCA is just CA applied to dummies
    mca = prince.MCA(n_components=n_components,
                     n_iter=n_iter,
                     copy=copy,
                     check_input=check_input,
                     engine=engine,
                     random_state=random_state)

    mca = mca.fit(balloons)

    assert np.all(np.isclose(mca.eigenvalues_, ca.eigenvalues_))
    assert np.all(np.isclose(mca.col_masses_, ca.col_masses_))
    assert np.all(np.isclose(mca.row_masses_, ca.row_masses_))
    assert np.all(np.isclose(mca.explained_inertia_, ca.explained_inertia_))
    assert np.isclose(mca.total_inertia_, ca.total_inertia_)
    assert np.all(np.isclose(mca.column_coordinates(balloons), ca.column_coordinates(one_hot)))

    # MCA is PCA applied to the Transformed Complete Disjunctive Table (CDT)
    # TODO: Finish this ^^
    CDT = pd.get_dummies(balloons)
    p = CDT.sum(axis=0) / len(CDT)
    X = CDT / p  # CDT / p has expected value = 1

    pca = PCA(rescale_with_mean=False,
              rescale_with_std=False,
              n_components=n_components,
              n_iter=n_iter,
              copy=copy,
              check_input=check_input,
              random_state=random_state,
              engine=engine)

    print(np.corrcoef(X))
    print(S)
