import numpy as np
import prince

from statistics_utils.sample_data.wine_ratings import wine_ratings

if __name__ == "__main__":
    n_components = 2
    n_iter = 3
    copy = True
    check_input = True
    engine = "auto"
    random_state = 42

    famd = prince.FAMD(n_components=n_components,
                       n_iter=n_iter,
                       copy=copy,
                       check_input=check_input,
                       engine=engine,
                       random_state=random_state)

    famd = famd.fit(wine_ratings)

    # FAMD is just MFA applied to categorical and numerical variables as the two groups
    groups = {"Categorical": ["E1 fruity", "E1 woody", "E1 coffee"],
              "Numerical": ["E2 red fruit", "E2 roasted", "E2 vanillin", "E2 woody",
                            "E3 fruity", "E3 butter", "E3 woody"]}

    mfa = prince.MFA(groups=groups,
                     n_components=n_components,
                     n_iter=n_iter,
                     copy=copy,
                     check_input=check_input,
                     engine=engine,
                     random_state=random_state)

    mfa.fit(wine_ratings)

    assert np.all(np.isclose(mfa.row_coordinates(wine_ratings), famd.row_coordinates(wine_ratings)))
    assert np.all(np.isclose(mfa.eigenvalues_, famd.eigenvalues_))
