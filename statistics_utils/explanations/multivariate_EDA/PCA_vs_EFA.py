import matplotlib.pyplot as plt
import numpy as np
from statsmodels.multivariate.factor import Factor
from statsmodels.multivariate.pca import PCA

from statistics_utils.sample_data.generate_regression_data import generate_x_norm


def _print_header(header: str):
    print("\n" + "-" * 100)
    print(header)
    print("-" * 100)


def _print_subheader(subheader: str):
    print("\n" + subheader)
    print("-" * len(subheader))


def _plot_correlation_matrix(corr_matrix: np.ndarray):
    n_x = len(corr_matrix)
    plt.imshow(corr_matrix, cmap="coolwarm", vmin=-1, vmax=1)
    plt.gca().xaxis.set_ticks_position('top')
    plt.xticks(range(n_x))
    plt.yticks(range(n_x))
    plt.show()


if __name__ == "__main__":
    np.random.seed(1234567890)
    n_x = 4
    n_hidden_factors = 3
    n_obs = 10000
    mu = 0
    sd = 1
    x_df = generate_x_norm(n_x=n_x, n_obs=n_obs, mus=[mu] * n_x, sds=[1., 1., 0.1, 0.1])

    _print_header("Data")
    print(x_df.head())

    _plot_correlation_matrix(x_df.corr())

    # 1. PCA on standardized centered data
    _print_header("PCA on standardized centered data")
    pca = PCA(data=x_df, standardize=True, demean=True, normalize=False)
    x_df_transformed = (x_df - np.mean(x_df, axis=0)) / np.sqrt(np.var(x_df, axis=0))
    assert np.all(np.isclose(pca.transformed_data, x_df_transformed))
    factors = pca.factors
    explained_variances = np.var(factors, axis=0)
    _print_subheader("Loadings PCA (=coeffs of hidden variables w.r.t. original)")
    print(pca.loadings.iloc[:, :n_hidden_factors])

    # 2. EFA -- this is implicitly done on standardized data (reps. on correlation matrix)
    # This tries to fit model
    # y = Lamba @ eta + epsilon
    # assuming
    # 1) eta ~ N(0, Psi)
    # 2) epsilon ~ N(0, Theta)
    # 3) cov(eps, eta) = 0
    # The shape of Lambda is (# orig params) x (# hidden params = n_factor)
    # Note: The n_factor in the result does not have to be the n_factor in the constructor
    # statsmodels.multivariate.factor.Factor, since this depends also on the input data structure
    # and algoritm used

    # Additionally to identify the model we assume
    # Psi = Identity matrix
    # By default section 11.2 from http://www.openaccesstexts.org/pdf/Quant_Chapter_11_efa.pdf
    # is used as algorithm to estimate the model
    _print_header("Exploratory Factor Analysis")
    fa = Factor(x_df, n_factor=n_hidden_factors, corr=None, method="pa", smc=True)
    fa_result = fa.fit()
    assert np.all(np.isclose(fa.corr, x_df.corr()))
    fitted_cov = fa_result.loadings @ fa_result.loadings.T + np.diag(fa_result.uniqueness)
    assert np.all(np.isclose(fitted_cov, fa_result.fitted_cov))  # \hat(R) = Lambda @ Lambda.T + Theta
    assert np.all(np.isclose(fa_result.uniqueness, 1 - fa_result.communality))
    assert np.all(np.isclose(fa_result.communality, fa.communality))
    assert np.all(np.isclose(fa_result.loadings, fa.loadings))
    # _print_subheader("Difference between sample and fitted correlation")
    # print(fitted_cov - fa.corr)

    fa_result.rotate(method="varimax")
    C = fa_result.rotation_matrix
    Lambda = fa_result.loadings_no_rot
    assert np.all(np.isclose(Lambda @ C, fa_result.loadings))

    _print_subheader("Loadings EFA (=coeffs of hidden variables w.r.t. original)")
    print(fa_result.loadings)
