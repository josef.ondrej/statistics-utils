Multivariate Exploratory Data Analysis
======================================

The motivation here is the question [what is the analogy of PCA for mixed data?](https://stats.stackexchange.com/questions/5774/can-principal-component-analysis-be-applied-to-datasets-containing-a-mix-of-cont)

The answer depends on:  
- what types of variables you have (numerical / categorical)
- how are they structured
- how are your observatios structured 

All is nicely summarized in the docs for the `R` package `FactoMineR` at http://factominer.free.fr/factomethods/index.html and also 
in the `Guidelines` section of the `Prince` package at https://github.com/MaxHalford/prince:
- one set of individuals, one set of variables of the same type:
  - use [Principal Component Analysis](https://en.wikipedia.org/wiki/Principal_component_analysis) for numerical data
  and [Multiple Correspondence Analysis](https://en.wikipedia.org/wiki/Multiple_correspondence_analysis) for categorical data ([Correspondence Analysis](https://en.wikipedia.org/wiki/Correspondence_analysis) for data in form of contingency table)
- one set of individuals, two types of variables (numerical + categorical:
  - use [Factor Analysis of Mixed Data](https://en.wikipedia.org/wiki/Factor_analysis_of_mixed_data)
- one set of individuals, several sets of variables (each either cathegorical <b>or</b> numerical):
  -  use [Multiple Factor Analysis](https://en.wikipedia.org/wiki/Multiple_factor_analysis)

Do not confuse the methods above with [Factor Analysis](https://en.wikipedia.org/wiki/Factor_analysis) resp. [Exploratory Factor Analysis](https://en.wikipedia.org/wiki/Exploratory_factor_analysis). For the comparison of Factor Analysis with PCA (which is a special case of the previously mentioned methods) see: https://en.wikipedia.org/wiki/Factor_analysis#Exploratory_factor_analysis_versus_principal_components_analysis
The factor analysis implemented in `statsmodels` package is described in chapters 9 and 11 here http://www.openaccesstexts.org/download.php.

In case you want to employ these techniques for dimensionality reduction, it might be useful to check:
- https://en.wikipedia.org/wiki/Dimensionality_reduction

Books:
- Pagès Jérôme (2014). Multiple Factor Analysis by Example Using R. Chapman & Hall/CRC The R Series London
