import prince

from statistics_utils.sample_data.wine_ratings import wine_ratings

if __name__ == "__main__":
    n_components = 2
    n_iter = 3
    copy = True
    check_input = True
    engine = "auto"
    random_state = 42

    groups = {"Expert #1": ["E1 fruity", "E1 woody", "E1 coffee"],
              "Expert #2": ["E2 red fruit", "E2 roasted", "E2 vanillin", "E2 woody"],
              "Expert #3": ["E3 fruity", "E3 butter", "E3 woody"]}

    mfa = prince.MFA(groups=groups,
                     n_components=n_components,
                     n_iter=n_iter,
                     copy=copy,
                     check_input=check_input,
                     engine=engine,
                     random_state=random_state)

    mfa.fit(wine_ratings)

    # MFA is calculated in the following way
    # ---------------------------------------------------------------------------------------
    # 1. do PCA or MCA on each group, save the largest eigenvalue from that
    #
    # 2. for each group divide either the matrix of variable values (for continuous groups)
    #    OR the matrix of dummy variables (for categorical groups) by that eigenvalue
    #
    # 3. Concatenate these matrices and do a PCA on them
