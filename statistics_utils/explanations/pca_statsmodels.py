import numpy as np
import sklearn.decomposition
from statsmodels.multivariate.pca import PCA

from statistics_utils.sample_data.generate_regression_data import generate_x_norm

if __name__ == "__main__":
    n_obs = 1000
    dim_x = 4
    x_data = generate_x_norm(n_x=dim_x, n_obs=n_obs, mus=[2, 5, 10, 11], sds=[3, 4, 2, 1])

    # PCA in statsmodels
    pca = PCA(data=x_data, standardize=False, demean=True, normalize=False)

    # The pca_statsmodels.eigenvecs resp. loadings is an orthonormal basis
    assert np.all(np.isclose(pca.eigenvecs, pca.loadings))
    assert np.all(np.isclose(pca.coeff, pca.eigenvecs.T))
    assert np.all(np.isclose(pca.eigenvecs.T @ pca.eigenvecs, np.eye(dim_x)))
    assert np.all(np.isclose(pca.coeff.T @ pca.coeff, np.eye(dim_x)))

    # based on the standardize / demean arguments the data are transformed before going to the true PCA
    x_data_transformed = x_data - np.mean(x_data, axis=0)
    # x_data_transformed = x_data

    # .transformed_data just gives us the data on which the PCA was really applied to
    assert np.all(np.isclose(x_data_transformed, pca.transformed_data))

    # The factors are the original data written in the base of .eigenvecs.T
    assert np.all(np.isclose(np.matmul(pca.factors, pca.eigenvecs.T) + np.array(np.mean(x_data, axis=0)), x_data))
    assert np.all(np.isclose(np.matmul(x_data_transformed, pca.eigenvecs), pca.factors))

    # Compare with sklearn's PCA
    # Note that the pca base is unique up to a sign
    sklearn_PCA = sklearn.decomposition.PCA()
    sklearn_factors = sklearn_PCA.fit_transform(x_data)
    sklearn_eigenvecs = sklearn_PCA.components_.T

    assert np.all(np.isclose(np.abs(sklearn_factors), np.abs(pca.factors)))
    assert np.all(np.isclose(np.abs(sklearn_eigenvecs), np.abs(pca.eigenvecs)))
