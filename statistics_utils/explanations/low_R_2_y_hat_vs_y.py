import matplotlib.pyplot as plt
import numpy as np
import statsmodels.api as sm
from pandas import DataFrame

# Set parameters
n, k = 1000, 2
beta = np.array([3.0, 1.8, 2.3]).reshape(-1, 1)
sd = 10.0

# Generate data
np.random.seed(123456789)
x = np.random.randn(n, k)
x_intercept = np.c_[np.ones(n), x]
eps = np.random.randn(n) * sd
mu = np.matmul(x_intercept, beta).ravel()
y = mu + eps

data_dict = {"y": y}
data_dict.update({f"x_{index + 1}": col for index, col in enumerate(x.T)})
data = DataFrame(data=data_dict)
print(data.head())

# Fit model
formula = f"y ~ {' + '.join([f'x_{i + 1}' for i in range(x.shape[1])])}"
model = sm.OLS.from_formula(formula=formula, data=data)
model_result = model.fit()
fitted_values = model_result.fittedvalues

# Plot fitted vs observed values
plt.figure(figsize=(7, 7))
plt.scatter(y, fitted_values, color="deepskyblue")
axis_limit = min(min(y), min(fitted_values)), max(max(y), max(fitted_values))
plt.ylim(axis_limit)
plt.xlim(axis_limit)
plt.plot(axis_limit, axis_limit, color="gray", linestyle="dashed")
plt.xlabel("Observed")
plt.ylabel("Fitted")
plt.title("Fitted ~ Observed")
plt.grid()
plt.show()
