import numpy as np
import statsmodels.api as sm

from statistics_utils.sample_data.generate_regression_data import generate_y_x

if __name__ == "__main__":
    # Coefficient values from LASSO are NOT equal to coefficients of model with less covariates
    data = generate_y_x(beta=[1., 2., 3., 4.])
    data["r"] = np.random.randn(len(data))

    model_lasso = sm.OLS.from_formula(formula="y ~ x_0 + x_1 + x_2 + r", data=data)
    model_lasso_result = model_lasso.fit_regularized(L1_wt=1.0, alpha=1.0)

    print("LASSO params\n" + "-" * 50 + "\n")
    print(model_lasso_result.params)

    model = sm.OLS.from_formula(formula="y ~ -1 + x_0 + x_1 + x_2", data=data)
    model_result = model.fit()

    print("\nReduced model params\n" + "-" * 50 + "\n")
    print(model_result.params)
