import numpy as np
from sklearn.decomposition import PCA

from statistics_utils.sample_data.generate_regression_data import generate_x_norm

if __name__ == "__main__":
    n_obs = 1000
    dim_x = 4
    x_data = generate_x_norm(n_x=dim_x, n_obs=n_obs, mus=[2, 5, 10, 11], sds=[3, 4, 2, 1])

    # PCA in sklearn
    pca = PCA()
    transformed = pca.fit_transform(x_data)

    # The pca_sklearn.mean_ is just the column mean of x_data
    assert np.all(pca.mean_ == np.mean(x_data, axis=0))

    # The transformed data has the same shape as the original
    assert np.all(x_data.shape == transformed.shape)

    # The rows of components are orthonormal vectors from the same space as rows of x_data
    assert np.all(np.isclose(np.sum(pca.components_ ** 2, axis=1), np.array([1] * dim_x)))
    assert pca.components_.shape[1] == x_data.shape[1]

    # The transformed data is the centered original data written in the basis of the components
    assert np.all(np.isclose((x_data - pca.mean_) @ pca.components_.T, transformed))
    # This is how to rewrite the transformed in the original basis (and add the mean)
    assert np.all(np.isclose(transformed @ pca.components_ + pca.mean_, x_data))

    # The explained variance is the sample variance of each PCA component
    assert np.all(np.isclose(np.var(transformed, axis=0) * (n_obs / (n_obs - 1)), pca.explained_variance_))

    # Orthonormal basis of an inner product space V
    # is a set of vectors of unit norm which are orthogonal
    # If we have vector x in V that can be written as
    # x = a * u1 + b * u2 + c * u3
    # where U = (u1, u2, u3) is the orthonormal basis
    # then we can calculate the coordinates (a, b, c) of x in U as
    # (u1 * x, u2 * x, u3 * x), where * is the inner product
    #
    # Example of writing vector x in the coordinates of basis U
    U = np.array([[0, 1, 0],
                  [1, 0, 0],
                  [0, 0, 1]])

    x = np.array([2, 3, 4])

    assert np.all(x @ U.T == np.array([3, 2, 4]))
