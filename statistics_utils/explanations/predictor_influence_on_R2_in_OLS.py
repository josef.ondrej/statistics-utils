import pandas as pd
from statsmodels.multivariate.pca import PCA
from statsmodels.regression.linear_model import OLS

from statistics_utils.sample_data.generate_regression_data import generate_y_x

if __name__ == "__main__":
    # It is generally not true that
    # (RSS full model without predictor x - RSS full model) = (RSS intercept model - RSS intercept model with predictor x)
    # However if we use PCA (replace_x_by_its_pca = True) as the predictors, then this seems to be the case
    # Observations:
    #    - PCA columns have mean zero (this is because they are just some linear combination of zero mean columns -
    #                                  remember we center the original columns before applying PCA), so they are orthogonal
    #                                  to the intercept 
    #    - PCA columns are orthogonal (their dot product is zero -- try factors.T @ factors), this is because
    #                                  X @ D.T = factors, so using the annotation from SVD factors.T @ factors should just
    #                                  be (D @ X.T) @ (X @ D.T) = D @ (X.T @ X) S = D @ (S @ V @ D) @ S = V - which is diagonal
    #    - the explanation should then follow from Komarek - theorem 6.3
    data = generate_y_x(beta=[2.0, 1.4, 3.5, 4.2], n_obs=100)
    replace_x_by_its_pca = True

    if replace_x_by_its_pca:
        x_col_names = ["x_0", "x_1", "x_2"]
        y_col_name = "y"
        pca = PCA(data=data[x_col_names], demean=True, standardize=False, normalize=False)
        factors = pca.factors
        factors.columns = x_col_names
        data = pd.concat([data[y_col_name], factors], axis=1)

    formulas = ["y ~ 1 + x_0 + x_1 + x_2",
                "y ~ 1 + x_0 + x_1",
                "y ~ 1 + x_2",
                "y ~ 1"]

    models = [OLS.from_formula(formula=formula, data=data) for formula in formulas]
    model_results = [model.fit() for model in models]

    for model_result in model_results:
        formula = model_result.model.formula
        spacing = 40 - len(formula)
        print(f"RSS for model {model_result.model.formula}: {model_result.ssr:>{spacing}.8}")

    full_model_rss = model_results[0].ssr
    full_model_without_x2_rss = model_results[1].ssr
    model_intercept_x2_rss = model_results[2].ssr
    model_intercept_rss = model_results[3].ssr

    print("-" * 100)

    print(f"RSS full model without x2 - RSS full model:       {full_model_without_x2_rss - full_model_rss}")
    print(f"RSS intercept model - RSS intercept + x2 model:   {model_intercept_rss - model_intercept_x2_rss}")
