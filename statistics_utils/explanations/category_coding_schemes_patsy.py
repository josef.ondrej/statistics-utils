import pandas as pd
from statsmodels.genmod.families import Gaussian
from statsmodels.genmod.families.tests.test_link import identity
from statsmodels.genmod.generalized_linear_model import GLM

from statistics_utils.sample_data.generate_regression_data import generate_y_category
from statistics_utils.tests.test_estimable_parameters import test_estimable_parameters
from statistics_utils.tests.z_test import z_test_results_to_str

if __name__ == "__main__":
    # Generate cathegorical data
    category_names = [1, 2, 3, 4]
    category_means = [0.5, 0.5, 0.5, 5.0]
    category_to_mean = {name: mu for name, mu in zip(category_names, category_means)}

    y, categories = generate_y_category(category_means=category_to_mean)
    data = pd.DataFrame({"y": y, "cat": categories})

    # Estimate basic model. Intercept represents the mean of the reference cathegory = 1
    # The coefficients for cathegories 2,3,4 represent the difference of those cathegories from cathegory one
    model = GLM.from_formula("y ~ C(cat, Treatment  (reference=1))", data=data, family=Gaussian(link=identity))
    model_result = model.fit()
    print(model_result.summary())

    ####################################################################################################################
    # 1. Treatment without intercept                                                                                   #
    # The interpretation of the coefficients for cathegories is that each coefficient is just the mean of that         #
    # cathegory                                                                                                        #
    ####################################################################################################################
    model_treatment_no_intercept = GLM.from_formula("y ~ -1 + C(cat, Treatment)", data=data,
                                                    family=Gaussian(link=identity))
    model_treatment_no_intercept_result = model_treatment_no_intercept.fit()
    print(model_treatment_no_intercept_result.summary())

    parameter_beta_coefficients = [[1, 0, 0, 0], [1, 1, 0, 0], [1, 0, 1, 0], [1, 0, 0, 1]]
    parameter_names = ["cat_1", "cat_2", "cat_3", "cat_4"]

    beta_estimates = model_result.params
    beta_information_matrix = model.information(model_result.params)
    z_test_results = test_estimable_parameters(beta_estimates, beta_information_matrix, parameter_beta_coefficients)
    summary = z_test_results_to_str(z_test_results, parameter_names)
    print(summary)

    ####################################################################################################################
    # 2. Sum without intercept                                                                                         #
    # The first coefficient represents the grand mean (mu) and the other represent (mu cathegory - mu)                 #
    # Not all the cathegories are present - one must be ommited                                                        #
    ####################################################################################################################
    model_sum_no_intercept = GLM.from_formula("y ~ -1 + C(cat, Sum(omit=-1))", data=data,
                                              family=Gaussian(link=identity))
    model_sum_no_intercept_result = model_sum_no_intercept.fit()
    print(model_sum_no_intercept_result.summary())

    parameter_beta_coefficients = [[0.25, 0.25, 0.25, 0.25],
                                   [0.75, -0.25, -0.25, -0.25], [-0.25, 0.75, -0.25, -0.25],
                                   [-0.25, -0.25, 0.75, -0.25], [-0.25, -0.25, -0.25, 0.75]]
    parameter_names = ["mu of all", "cat_1 - mu", "cat_2 - mu", "cat_3 - mu", "cat_4 - mu"]

    beta_estimates = model_treatment_no_intercept_result.params  # !!! WARNING !!! -- note that these parameters are
    # from model_treatment_no_intercept_result and represent
    # the cathegory means
    beta_information_matrix = model_treatment_no_intercept.information(model_treatment_no_intercept_result.params)
    z_test_results = test_estimable_parameters(beta_estimates, beta_information_matrix, parameter_beta_coefficients)
    summary = z_test_results_to_str(z_test_results, parameter_names)
    print(summary)
