import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.genmod.families import Gaussian
from statsmodels.genmod.families.tests.test_link import identity

if __name__ == "__main__":
    # How does OLS behave when x is specified as continuous but in fact has only a few values
    n_obs = 100000
    data = pd.DataFrame(data=[{"x": 1}] * n_obs + [{"x": 2}] * n_obs)
    data["y"] = np.random.randn(2 * n_obs)
    print(data)

    model = sm.GLM.from_formula(formula="y ~ x", data=data, family=Gaussian(link=identity))
    model_result = model.fit()
    print(model_result.summary())
