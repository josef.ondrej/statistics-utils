import numpy as np
from statsmodels.multivariate.pca import PCA

from statistics_utils.sample_data.generate_regression_data import generate_x_norm

if __name__ == "__main__":
    n_obs = 1000
    dim_x = 4
    x_data = generate_x_norm(n_x=dim_x, n_obs=n_obs, mus=[2, 5, 10, 11], sds=[3, 4, 2, 1])

    # Do statsmodels PCA just to check
    pca = PCA(data=x_data, standardize=False, demean=True, normalize=False)

    pca_basis_vectors = pca.coeff  # orthonormal basis vectors (rows)
    x_data_in_pca_basis = pca.factors  # row vectors of x_data written in pca_basis_vectors

    x_data_centered = x_data - np.mean(x_data, axis=0)
    assert np.all(np.isclose(np.matmul(x_data_centered, pca_basis_vectors.T), x_data_in_pca_basis))

    # Manual calculation
    covariance_matrix = np.cov(x_data_centered.T)
    assert np.all(np.isclose(x_data_centered.T @ x_data_centered / (n_obs - 1), covariance_matrix))
    S, V, D = np.linalg.svd(covariance_matrix)

    # Because covariance matrix is symemtric, then S.T = D
    assert np.all(np.isclose(S.T, D))

    # D is the pca basis (up to a sign)
    assert np.all(np.isclose(np.abs(D), np.abs(pca_basis_vectors)))

    # V is the vector of sample variances of the coordinates of the components, e.g.
    assert np.all(np.isclose(np.sum(x_data_in_pca_basis ** 2, axis=0) / (n_obs - 1), V))
    assert np.all(np.isclose(np.var(x_data_in_pca_basis, axis=0) * (n_obs / (n_obs - 1)), V))
