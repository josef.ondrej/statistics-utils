import numpy as np
import pandas as pd
from statsmodels.genmod.families import Gaussian
from statsmodels.genmod.families.tests.test_link import identity
from statsmodels.genmod.generalized_linear_model import GLM

from statistics_utils.sample_data.generate_regression_data import generate_y_category
from statistics_utils.tests.test_estimable_parameters import test_estimable_parameters
from statistics_utils.tests.z_test import z_test_results_to_str

if __name__ == "__main__":
    # The problem here is the following. Given a cathegorical predictor with say 4 cathegories, we want to test which
    # of the cathegory levels are significant. We can do this in two ways.
    # A) take model y ~ 1 + Indicator(category = level) and take a look at the significance of the level coefficient
    # B) take model y ~ Indicator(category = level_1) + Indicator(category = level_2) + ... + Indicator(category = level_4)
    #    and then take a look at the significance of estimable parameter theta, where theta = beta_level - (sum_{i != level} beta_i) / 3
    # As the following example shows, these two approaches test two very different things in an unbalanced design.
    # In the following all the cathegories are significantly different from each other, but this will not show up
    # in the approach B). Of course counterexample for A) also exists, so in this light it is best to include all the
    # levels (even if not significant) if the cathegorical variable is significant as whole.
    print_estimated_category_means_summary = False

    # Prepare data
    np.random.seed(123456)
    n_obs = 100
    category_names = [1, 2, 3, 4]
    category_means = [0.0, 0.0, 10.0, 10.0]
    category_probas = [0.02, 0.02, 0.48, 0.48]

    category_to_mean = {name: mu for name, mu in zip(category_names, category_means)}
    y, categories = generate_y_category(category_means=category_to_mean, category_probas=category_probas, n_obs=n_obs)
    data = pd.DataFrame({"y": y, "cat": categories})
    for category_name in category_names:
        data[f"cat_{category_name}"] = 1.0 * (data["cat"] == category_name)

    # 1. Basic category dummy encoding model withou intercept
    model = GLM.from_formula("y ~ -1 + C(cat, Treatment)", data=data, family=Gaussian(link=identity))
    model_result = model.fit()
    if print_estimated_category_means_summary:
        print("Category means:")
        print("\n".join(str(model_result.summary()).split("\n")[-8:]))

    # 2. The same model as in 1. ^^ except we test category mean - mean of other category means
    parameter_beta_coefficients = [[1, -1 / 3, -1 / 3, -1 / 3],
                                   [-1 / 3, 1, -1 / 3, -1 / 3],
                                   [-1 / 3, -1 / 3, 1, -1 / 3],
                                   [-1 / 3, -1 / 3, -1 / 3, 1]]
    parameter_names = ["cat_1 - mu_rest",
                       "cat_2 - mu_rest",
                       "cat_3 - mu_rest",
                       "cat_4 - mu_rest"]

    beta_estimates = model_result.params
    beta_information_matrix = model.information(model_result.params)
    z_test_results = test_estimable_parameters(beta_estimates, beta_information_matrix, parameter_beta_coefficients)
    summary = z_test_results_to_str(z_test_results, parameter_names)
    print("\nCategory mean - the mean of other cathegory means: ")
    print(summary)

    # 3. Category mean vs the mean of the other categories (!= mean of other category means)
    cat_name = 4
    model_indicator_cat = GLM.from_formula(f"y ~ cat_{cat_name}", data=data, family=Gaussian(link=identity))
    model_indicator_cat_result = model_indicator_cat.fit()
    print(f"\nCategory {category_name} vs the other cathegories")
    print("\n".join(str(model_indicator_cat_result.summary()).split("\n")[-6:]))
