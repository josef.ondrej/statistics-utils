from typing import List, Dict

import numpy as np


def get_lincomb_coeff_for_cat_mean_vs_mean_of_rest(category_names: List[str], model_coeff_names: List[str],
                                                   intercept_name: str = "Intercept") -> Dict[str, List[float]]:
    lincomb_coeffs_for_category_mean = get_lincomb_coeffs_for_category_mean(category_names, model_coeff_names,
                                                                            intercept_name)
    cat_name_to_cat_coeffs: Dict[str, List[float]] = {}
    for category_name in category_names:
        cat_coeffs = np.array(lincomb_coeffs_for_category_mean[category_name])
        mean_of_other_cats_coeffs = np.array([lincomb_coeffs_for_category_mean[name]
                                              for name in category_names if name != category_name]).mean(axis=0)
        coeffs = list(cat_coeffs - mean_of_other_cats_coeffs)
        new_cat_name = f"{category_name} - mu"
        cat_name_to_cat_coeffs[new_cat_name] = coeffs

    return cat_name_to_cat_coeffs


def get_lincomb_coeffs_for_category_mean(category_names: List[str], model_coeff_names: List[str],
                                         intercept_name: str = "Intercept") -> Dict[str, List[float]]:
    cat_name_to_cat_coeffs: Dict[str, List[float]] = {}
    n_coeff = len(category_names)
    assert len(model_coeff_names) == len(category_names)
    intercept_index = model_coeff_names.index(intercept_name)
    last_category_found = False
    for category_name in category_names:
        coeffs = [0] * n_coeff
        coeffs[intercept_index] = 1
        if category_name in model_coeff_names:
            cat_index = model_coeff_names.index(category_name)
            coeffs[cat_index] = 1
        else:
            if not last_category_found:
                last_category_found = True
            else:
                raise AssertionError(
                    f"Category names contain more than one category ({category_name}) that is not in model coeff names.")
        cat_name_to_cat_coeffs[category_name] = coeffs

    return cat_name_to_cat_coeffs


def get_identical_coeff_lincombs(category_names: List[str]):
    n_params = len(category_names)
    eye = np.eye(n_params)
    name_to_coeff_lincombs = {name: list(row) for name, row in zip(category_names, eye)}
    return name_to_coeff_lincombs
