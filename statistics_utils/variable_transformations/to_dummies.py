from typing import Dict, Any, Tuple

import pandas as pd

from statistics_utils.utils.string_utils import sanitize


def get_dummies(series: pd.Series) -> Tuple[pd.DataFrame, Dict[Any, str]]:
    dummies = pd.get_dummies(series)
    prefix = sanitize(series.name)
    new_colnames = [f"{prefix}_{colname}" for colname in dummies.columns]
    cat_to_colname = {cat: colname for cat, colname in zip(dummies.columns, new_colnames)}
    dummies.columns = new_colnames
    return dummies, cat_to_colname


if __name__ == "__main__":
    series = pd.Series([1, 2, 3, 4, 4, 2, 3, 1, 1, 1, 2, 2, 2, 3, 4], name="cat")
    dummies, cat_to_colname = get_dummies(series)
    print(dummies)
    print(cat_to_colname)
