import pymc3 as pm
import theano.tensor as tt


class ZeroInflatedLognormal(pm.Lognormal):
    EPSILON = 1e-10

    def __init__(self, p_0=0.01, mu=0, sigma=None, tau=None, sd=None, *args, **kwargs):
        super().__init__(mu, sigma, tau, sd, *args, **kwargs)
        self.p_0 = p_0
        self.p_0_log = tt.log(p_0)
        self.q_0_log = tt.log(1 - p_0)

    def random(self, point=None, size=None):
        raw = super().random(point, size)
        Q = pm.Bernoulli.dist(1 - self.p_0).random(size=raw.shape)
        return raw * Q

    def logp(self, value):
        return tt.switch(tt.lt(value, self.EPSILON), self.p_0_log, self.q_0_log + super().logp(value))
