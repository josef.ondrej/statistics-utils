from pymc3 import Model
from pymc3.model import PyMC3Variable


def get_RV(name: str, model: Model) -> PyMC3Variable:
    matching_vars = [var for var in model.unobserved_RVs if var.name.endswith(name)]
    if len(matching_vars) > 1:
        raise AssertionError("Multiple variables matchings this name")

    if len(matching_vars) == 0:
        return None

    return matching_vars[0]
