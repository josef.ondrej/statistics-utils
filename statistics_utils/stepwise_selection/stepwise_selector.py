import os
import sys
from typing import List, Tuple

import numpy as np
from pandas import DataFrame

__transforms__ = ["log"]
__filters__ = ["R2"]

from patsy.desc import ModelDesc

from statsmodels.regression.linear_model import OLS

from send_to_slack.send import send_text


class StepwiseSelector:
    def __init__(self, interactions: int = 1,
                 transforms: List[str] = None,
                 filter: str = None,
                 filter_size: int = 150,
                 verbose: bool = True,
                 channel: str = None,
                 log_file_path: str = None):
        self._interactions = interactions
        self._transforms = transforms or []
        self._filter = filter
        self._filter_size = filter_size
        self._verbose = verbose
        self._channel = channel
        self._log_file_path = log_file_path
        try:
            os.remove(self._log_file_path)
        except:
            pass

        self._recursion_limit = 1500

    def _log(self, message: str):
        if self._log_file_path is not None:
            with open(self._log_file_path, "a") as log_file:
                log_file.write(message)

        if self._verbose:
            print(message)
            if self._channel is not None:
                send_text(message, channel=self._channel)


    def select(self, data: DataFrame, y_name: str, x_names: List[str]) -> List[Tuple[List[str], float]]:
        raise NotImplementedError("Has to be overriden")

    def _initialize(self, data: DataFrame, y_name: str, x_names: List[str]):
        pass

    def _expand_x(self, x_names: List[str]) -> List[str]:
        formula = f"({'+'.join(x_names)})**{self._interactions}"

        original_recursion_limit = sys.getrecursionlimit()
        sys.setrecursionlimit(self._recursion_limit)
        model_description = ModelDesc.from_formula(formula)
        sys.setrecursionlimit(original_recursion_limit)

        expanded_x_names = [term.name() for term in model_description.rhs_termlist]
        expanded_x_names.remove("Intercept")
        return expanded_x_names

    def _transform_x(self, data: DataFrame, y_name: str, x_names: List[str]) -> List[str]:
        """Apply different transformations on the predictors"""
        transformed_x_names = list()

        if "log" in self._transforms:
            for x_name in x_names:
                if (data[x_name] > 0).all():
                    transformed_x_names.append(f"np.log({x_name})")

        if "rank" in self._transforms:
            for x_name in x_names:
                rank_name = f"rank_{x_name}"
                transformed_x_names.append(rank_name)
                data[rank_name] = data[x_name].rank()

        poly_transforms = [trans for trans in self._transforms if trans.startswith("poly")]
        for transform in poly_transforms:
            _, exponent = transform.split("_")
            exponent = float(exponent)

            if exponent > 0 and np.isclose(int(exponent), exponent):
                for x_name in x_names:
                    exponent = int(exponent)
                    poly_x_name = f"np.power({x_name}, {exponent})"
                    transformed_x_names.append(poly_x_name)
            else:
                for x_name in x_names:
                    if (data[x_name] > 0).all():
                        poly_x_name = f"np.power({x_name}, {exponent})"
                        transformed_x_names.append(poly_x_name)

        self._log(f"[INFO] Added {len(transformed_x_names)} transformed predictors: {str(transformed_x_names)}")

        return transformed_x_names

    def _filter_x(self, data: DataFrame, y_name: str, x_names: List[str]) -> List[str]:
        """Preselect predictors based on some criteria"""
        if self._filter == "R2":
            R_squared_vector = [OLS.from_formula(formula=f"{y_name} ~ {x_name}", data=data).fit().rsquared for x_name in
                                x_names]
            order = np.argsort(-np.array(R_squared_vector))
            filtered_x_names = list(np.array(x_names)[order][:self._filter_size])
        else:
            filtered_x_names = x_names

        self._log(f"[INFO] Filtered {len(filtered_x_names)} predictors: {str(filtered_x_names)}")

        return filtered_x_names
