from typing import List

import numpy as np
from pandas import DataFrame
from patsy.desc import ModelDesc
from statsmodels.regression.linear_model import OLS
from tqdm import tqdm

from send_to_slack.send import send_text
from statistics_utils.stepwise_selection.stepwise_selector import StepwiseSelector


class ForwardBeamSelector(StepwiseSelector):
    def __init__(self, n_steps: int = 10, beam_width: int = 5, interactions: int = 1, transforms: List[str] = None,
                 filter: str = None, filter_size: int = 150, verbose: bool = True,
                 channel: str = "#automated-notifications"):
        self._n_steps = n_steps
        self._beam_width = beam_width
        super().__init__(interactions=interactions, transforms=transforms, filter=filter, filter_size=filter_size,
                         verbose=verbose, channel=channel)

    def _score(self, data: DataFrame, y_name: str, x_names: List[str]) -> float:
        raise NotImplementedError("Has to be overriden, "
                                  "for example implementation see ForwardBeamSelector._score_example")

    def _are_models_equivalent(self, model_1: List[str], model_2: List[str]) -> bool:
        return set(model_1) == set(model_2)

    def _on_substep_end(self, y_name: str, candidate_x_names: List[List[str]], candidate_scores: List[float], step: int,
                        substep: int):
        candidates_summary = "\n\t".join([f"{y_name} ~ {' + '.join(x_names)} (score = {score:.3f})"
                                          for x_names, score in zip(candidate_x_names, candidate_scores)])
        message = f"Best models so far (step={step + 1} / {self._n_steps}, " \
                  f"substep={substep + 1}/{self._beam_width if step > 0 else 1}): " \
                  f"\n\t{candidates_summary}"
        if self._channel is not None:
            send_text(text=f"```{message}```", channel=self._channel)

        with open("C:/Temp/log.txt", "a") as log:
            log.write("\n" + "-" * 100 + "\n")
            log.write(message)

    def select_best(self, data: DataFrame, y_name: str, x_names: List[str]):
        """Selects select_n best predictors to get best submodel of
        y_name ~ ('+'.join(transform_x(x_names)))**interactions

        Args:
            data: pandas Dataframe with column names that contain y_name and x_names

        Returns:
            List of transforms and interactions of x_names
            e.g. if x = ["x_1", "x_2", "x_3"] the result can be ["np.log(x_1):x_2", "x_3"]
        """
        self._initialize(data=data, y_name=y_name, x_names=x_names)

        transformed_x_names = x_names + self._transform_x(data, y_name, x_names)
        transformed_x_names_with_interactions = self._expand_x(transformed_x_names)
        filtered_transformed_x_names_with_interactions = self._filter_x(data, y_name,
                                                                        transformed_x_names_with_interactions)

        model_candidates = [[]] * self._beam_width

        for step in range(self._n_steps):
            print(f"Step: {step + 1} / {self._n_steps}");
            new_model_candidates = [[]] * self._beam_width
            new_model_scores = [float("-inf")] * self._beam_width
            for model_candidate_index, model_candidate in enumerate(model_candidates):
                if model_candidate_index > 0 and step == 0:
                    continue
                print(f"\tSubstep: {model_candidate_index + 1}/{self._beam_width if step > 0 else 1}")
                for x_name in tqdm(filtered_transformed_x_names_with_interactions):
                    if x_name in model_candidate:
                        continue  # do not try already selected variables

                    new_model_candidate = model_candidate + [x_name]

                    if any(self._are_models_equivalent(new_model_candidate, cand) for cand in new_model_candidates):
                        continue  # do not try already selected models

                    new_model_score = self._score(data, y_name, new_model_candidate)
                    if new_model_score > min(new_model_scores):
                        replace_index = np.argmin(new_model_scores)
                        new_model_candidates[replace_index] = new_model_candidate
                        new_model_scores[replace_index] = new_model_score

                self._on_substep_end(y_name=y_name,
                                     candidate_x_names=new_model_candidates,
                                     candidate_scores=new_model_scores,
                                     step=step,
                                     substep=model_candidate_index)

            model_candidates = new_model_candidates

        return model_candidates


if __name__ == '__main__':
    data = DataFrame([
        [0.37, 1.41, 1.73, 0.37],
        [-1.17, -0.06, -0.49, -1.04],
        [0.64, 0.99, 0.92, 0.17],
        [1.22, -0.39, 0.86, -0.49],
        [-0.25, -1.61, -0.03, -0.12],
        [-0.76, -0.05, -1.48, -0.7],
        [-0.98, -2.01, 1.56, 0.98],
        [-1.86, 1.4, -0.17, 1.61],
        [-0.46, -0.22, -0.29, -0.67],
        [-2.1, -1.19, 0.48, -0.13],
        [-0.97, -0.15, 1.85, 1.22],
        [0.12, -1.3, -0.32, 0.68],
        [0.04, 0.42, -0.88, 0.22],
        [-0.99, -0.14, 1.91, 0.23],
        [-1.22, -0.5, 0.74, -0.95],
        [-0.55, 0.75, -1.64, 0.46],
        [-0.7, 0.79, -2.09, 0.45],
        [-1.29, -0.28, -1.17, -1.73],
        [0.16, -0.85, -1.2, 0.13],
        [-0.42, -0.82, 0.13, 1.23]])

    y_name = "y"
    x_names = ["x_1", "x_2", "x_3"]
    data.columns = [y_name] + x_names
    print(data.head())

    selector = ForwardBeamSelector(n_steps=5, interactions=2, beam_width=5)

    best_x_names = selector.select_best(data=data, y_name=y_name, x_names=x_names)
    print(best_x_names)
