from typing import List, Callable, Tuple

import matplotlib.pyplot as plt
from statsmodels.regression.linear_model import OLS

from statistics_utils.sample_data.generate_regression_data import generate_y_x


class Selector:
    def __init__(self, response_name: str, variable_names: List[str],
                 quality_evaluation_function: Callable[[str], float]):
        self._response_name = response_name
        self._variable_names = variable_names
        self._quality_evaluation_function = quality_evaluation_function

    def forward_selection_step(self, base_formula: List[str] = ["1"]) -> Tuple[str, float]:
        highest_score = float("-inf")
        highest_score_arg = None

        for variable_name in self._variable_names:
            if variable_name not in base_formula:
                formula = self._response_name + "~ " + "+".join(base_formula + [variable_name])
                formula_score = self._quality_evaluation_function(formula)
                if formula_score > highest_score:
                    highest_score = formula_score
                    highest_score_arg = variable_name

        return (highest_score_arg, highest_score)

    def forward_selection(self, base_formula: List[str] = ["1"], max_steps: int = None, verbose: bool = True) -> List[
        Tuple[str, float]]:
        if verbose:
            print(f"[INFO] Starting forward selection from baseline formula "
                  f"{self._response_name} ~ {' + '.join(base_formula)}")

        variable, score = self.forward_selection_step(base_formula)
        history = list()
        formula_variables = list(base_formula)

        while variable is not None and (max_steps is None or (max_steps is not None and len(history) < max_steps)):
            if verbose:
                print(f"\t selected {variable} [{score:.3f}]")
            history.append((variable, score))
            formula_variables.append(variable)
            variable, score = self.forward_selection_step(base_formula=formula_variables)

        return history

    def plot_history(self, history: List[Tuple[str, float]]):
        x_ax = list(range(len(history)))
        plt.plot(x_ax, [score for var_name, score in history])
        plt.xticks(ticks=x_ax, labels=[var_name for var_name, score in history],
                   rotation=90)
        plt.xlabel("Variable added name")
        plt.ylabel("Score value")
        plt.grid()
        plt.tight_layout()


if __name__ == "__main__":
    data = generate_y_x(beta=[1., 2., 3., 4., 5.])


    def evaluation_function(formula: str) -> float:
        model = OLS.from_formula(formula=formula, data=data)
        model_result = model.fit()
        return model_result.rsquared


    print(data.head())

    selector = Selector(response_name="y",
                        variable_names=["x_0", "x_1", "x_2", "x_3"],
                        quality_evaluation_function=evaluation_function)

    selector.forward_selection()
    selector.plot_history()
    plt.show()
