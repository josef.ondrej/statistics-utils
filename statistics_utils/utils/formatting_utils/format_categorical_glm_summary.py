from typing import List

import pandas as pd
from statsmodels.genmod.families import Gaussian
from statsmodels.genmod.families.tests.test_link import identity
from statsmodels.genmod.generalized_linear_model import GLM, GLMResults

from statistics_utils.sample_data.generate_regression_data import generate_y_category
from statistics_utils.tests.LR_test import likelihood_ratio_test_glm_model, LR_test_result_to_str
from statistics_utils.tests.test_estimable_parameters import test_estimable_parameters
from statistics_utils.tests.z_test import z_test_results_to_str
from statistics_utils.utils.string_utils import glm_result_to_str
from statistics_utils.variable_transformations.get_estimable_parameter_coefficients import get_identical_coeff_lincombs, \
    get_lincomb_coeffs_for_category_mean, get_lincomb_coeff_for_cat_mean_vs_mean_of_rest
from statistics_utils.variable_transformations.to_dummies import get_dummies

_mode_identity = "identity"
_mode_cat_vs_mean_of_other = "category_vs_mean_of_other"
_mode_cat = "category"
_estimable_parameter_modes = [_mode_identity,
                              _mode_cat_vs_mean_of_other,
                              _mode_cat]


def model_result_to_str(model_result: GLMResults, category_names: List[str] = None,
                        estimable_parameters_mode: str = _mode_identity, baseline_model_df: int = 0,
                        baseline_model_loglik: float = None):
    LR_test_result = likelihood_ratio_test_glm_model(model_result, baseline_model_df, baseline_model_loglik)
    LR_test_result_repr = LR_test_result_to_str(LR_test_result)

    if category_names is None:
        category_names = list(model_result.model.exog_names)

    if estimable_parameters_mode == _mode_identity:
        estimable_parameter_coeffs = get_identical_coeff_lincombs(category_names)
    elif estimable_parameters_mode == _mode_cat:
        estimable_parameter_coeffs = get_lincomb_coeffs_for_category_mean(category_names, model_result.model.exog_names)
    elif estimable_parameters_mode == _mode_cat_vs_mean_of_other:
        estimable_parameter_coeffs = get_lincomb_coeff_for_cat_mean_vs_mean_of_rest(category_names,
                                                                                    model_result.model.exog_names)
    else:
        raise ValueError(f"[ERROR] Unknown value {estimable_parameters_mode} for parameter estimable_parameters_mode.")

    estimable_params_names = list(estimable_parameter_coeffs.keys())
    estimable_params_beta_coeffs = [estimable_parameter_coeffs[name] for name in estimable_params_names]

    beta = model_result.params
    I_beta = model_result.model.information(beta)
    z_test_results = test_estimable_parameters(beta, I_beta, estimable_params_beta_coeffs)
    estimable_parameters_summary_repr = z_test_results_to_str(z_test_results, estimable_params_names)

    model_formula_repr = "-" * 90 + f"\nModel: {glm_result_to_str(model_result)}\n" + "-" * 90
    model_result_repr = model_formula_repr + "\n" + LR_test_result_repr + "\n" + estimable_parameters_summary_repr

    if estimable_parameters_mode == _mode_cat_vs_mean_of_other:
        model_result_repr += "\n\nLegend: \n\t mu ... mean of the remaining cathegories"

    return model_result_repr


if __name__ == "__main__":
    category_means = {1: 1.2, 2: 2.3, 3: 4.4}
    n_obs = 100
    y, cat = generate_y_category(category_means, n_obs)
    dummies, _ = get_dummies(cat)
    data = pd.concat([y, dummies], axis=1)

    category_names = list(data.columns)[1:]

    formula = "y ~ cat_1 + cat_2"
    model = GLM.from_formula(formula=formula, data=data, family=Gaussian(link=identity))
    model_result = model.fit()
    model_repr = model_result_to_str(model_result, category_names, estimable_parameters_mode=_mode_cat_vs_mean_of_other)
    print(model_repr)

    formula = "y ~ -1 + cat_1 + cat_2 + cat_3"
    model = GLM.from_formula(formula=formula, data=data, family=Gaussian(link=identity))
    model_result = model.fit()
    model_repr = model_result.summary()
    print(model_repr)
