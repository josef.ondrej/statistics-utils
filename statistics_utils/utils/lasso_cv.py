from typing import List, Tuple, Dict

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sklearn.linear_model
import statsmodels.api as sm
from patsy.highlevel import dmatrices
from sklearn.model_selection import cross_val_score

from statistics_utils.sample_data.generate_regression_data import generate_y_x
from statistics_utils.utils.statsmodel_wrapper import StatsmodelWrapper


def lasso_cv(data: pd.DataFrame, formula: str, alpha_grid: List[float] = None,
             L1_wt: float = 1.0, cv_folds: int = 5) -> List[Tuple[float, float]]:
    if alpha_grid is None:
        alpha_grid = [0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0, 10000.0, 100000.0]

    response_name = formula.split("~")[0].strip()

    alpha_score = list()

    for alpha in alpha_grid:
        elasticnet_kwargs = {"alpha": alpha, "L1_wt": L1_wt}
        sm_wrapper = StatsmodelWrapper(statsmodels_class=sm.OLS,
                                       formula=formula,
                                       fit_regularized=True,
                                       model_kwargs=None,
                                       fit_kwargs=elasticnet_kwargs)

        r2_scores = cross_val_score(sm_wrapper, data, data[response_name], cv=cv_folds, scoring="r2")
        avg_score = sum(r2_scores) / len(r2_scores)

        alpha_score.append((alpha, avg_score))

    return alpha_score


def lasso_cv_sklearn(data: pd.DataFrame, formula: str, cv: int = 5, max_iter: int = 1000) -> Dict:
    formula.split()
    y_name_raw, x_names_concat = formula.split("~")

    y_name = y_name_raw.strip()
    x_names = [x_name_raw.strip() for x_name_raw in x_names_concat.split("+")]

    # TODO: This is hacky, do it more cleanly
    if "-1" not in formula:
        formula += "-1"

    y, X = dmatrices(formula_like=formula, data=data)
    y = np.ravel(y)
    lasso_cv = sklearn.linear_model.LassoCV(max_iter=max_iter, cv=cv, fit_intercept=True)
    lasso_cv.fit(X, y)
    fitted_values = lasso_cv.predict(X)

    lasso_params = [lasso_cv.intercept_] + list(lasso_cv.coef_)
    param_names = ["Intercept"] + x_names

    return {"fit": lasso_cv,
            "fitted_values": fitted_values,
            "coeff": dict(zip(param_names, lasso_params)),
            "x_names": x_names,
            "y_name": y_name,
            "formula": formula,
            "data": data}


def summary_lasso_cv_sklearn(fit_result: Dict, plot_fit: bool = True):
    alpha = fit_result["fit"].alpha_
    sep = "=" * 50 + "\n"
    sumary = sep
    short_formula = fit_result['formula'][:50] + "..."
    sumary += f"Summary of {short_formula}\n"
    sumary += f"alpha: {alpha:.4f}\n"
    sumary += sep
    for param, value in fit_result["coeff"].items():
        if value != 0 or param == "Intercept":
            sumary += f"{param}:  {value:.8} \n"
    sumary += sep
    if plot_fit:
        observed = fit_result["data"][fit_result["y_name"]]
        predicted = fit_result["fitted_values"]
        plt.scatter(observed, predicted, alpha=0.2 * 1000 / max(1000, len(observed)))
        lb = min(min(observed), min(predicted))
        ub = max(max(observed), max(predicted))
        limits = [lb, ub]
        plt.plot(limits, limits, color="black", linestyle="dashed", alpha=0.8)
        plt.title(f"Observed - Fitted for LASSO model \n{short_formula}")
        plt.xlabel("Observed")
        plt.ylabel("Fitted")
        plt.show()
    return sumary


if __name__ == "__main__":
    data = generate_y_x(beta=[1., 2., 3., 4.])
    formula = "y ~ x_0 + x_1 + x_2"

    alpha_score = lasso_cv(data, formula)
    print(alpha_score)

    lasso_cv_data = lasso_cv_sklearn(data=data, formula=formula)
    print(summary_lasso_cv_sklearn(lasso_cv_data))
