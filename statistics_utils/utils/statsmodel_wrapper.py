from typing import Any, Dict

import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.model_selection import cross_val_score
from statsmodels.genmod.families import Gaussian
from statsmodels.genmod.families.tests.test_link import identity

from statistics_utils.sample_data.generate_regression_data import generate_y_x


class StatsmodelWrapper(BaseEstimator, RegressorMixin):
    def __init__(self, statsmodels_class, formula: str,
                 fit_regularized: bool = False,
                 model_kwargs: Dict[str, Any] = None, fit_kwargs: Dict[str, Any] = None):
        self._statsmodels_class = statsmodels_class
        self._formula = formula
        self._fit_regularized = fit_regularized
        self._model_kwargs = model_kwargs
        self._fit_kwargs = fit_kwargs

        self._model = None
        self._model_result = None

    def fit(self, X: pd.DataFrame, y: Any = None):
        model_kwargs = self._model_kwargs or dict()
        fit_kwargs = self._fit_kwargs or dict()

        self._model = self._statsmodels_class.from_formula(formula=self._formula, data=X, **model_kwargs)

        if self._fit_regularized:
            self._model_result = self._model.fit_regularized(**fit_kwargs)
        else:
            self._model_result = self._model.fit(**fit_kwargs)

    def predict(self, X: pd.DataFrame):
        return self._model_result.predict(X)

    def get_params(self, deep: bool = False) -> Dict[str, Any]:
        params = {"statsmodels_class": self._statsmodels_class,
                  "formula": self._formula,
                  "fit_regularized": self._fit_regularized,
                  "model_kwargs": self._model_kwargs,
                  "fit_kwargs": self._fit_kwargs}

        return params


if __name__ == "__main__":
    np.random.seed(123456789)

    data = generate_y_x(beta=[1., 2., 3., 4.])

    statsmodel_wrapper = StatsmodelWrapper(sm.GLM, "y ~ x_0 + x_1 + x_2",
                                           model_kwargs={"family": Gaussian(link=identity)})

    statsmodel_wrapper.fit(data)
    predicted = statsmodel_wrapper.predict(data)

    print(cross_val_score(statsmodel_wrapper, data, data["y"]))
    print(cross_val_score(statsmodel_wrapper, data, data["y"], scoring="r2"))

    # TODO: Make it work also with cathegorical data when some of the cathegories are missing
    # balloon_data = get_balloon_data()
    # balloon_data["y"] = 1 * (balloon_data["Inflated"] == "T")
    #
    # y, X = dmatrices(formula_like="y ~ C(Action)", data=balloon_data)
    #
    # formula = f"{y.design_info.column_names[0]} ~ {'+'.join(X.design_info.column_names)}"
    # formula = formula.replace("Intercept", "1")
    # model = sm.OLS.from_formula(formula, balloon_data)
