from typing import List

from statsmodels.genmod.generalized_linear_model import GLMResults


def sanitize(string: str) -> str:
    string = string.replace(".", "_")
    string = string.strip()
    string = string.lower()
    return string


def prettify_cathegory_name(string: str, length_without_suffix: int, suffix: str = None) -> str:
    cut_string = string[:length_without_suffix]
    str_len = len(cut_string)

    if str_len < length_without_suffix:
        cut_string += " " * (length_without_suffix - str_len)

    if suffix is not None:
        cut_string += suffix

    return cut_string


def get_significance_stars(p_val: float, alpha_levels: List[float] = None):
    if alpha_levels is None:
        alpha_levels = [.05, .01, .001]
    signif_stars = ""
    for alpha in alpha_levels:
        if alpha > p_val:
            signif_stars += "*"
    return signif_stars


def glm_result_to_str(glm_model_result: GLMResults, predictors_len: int = 30) -> str:
    model = glm_model_result.model
    response_name = model.formula.split("~")[0].strip()
    predictors = model.formula.split("~")[1].strip()
    family_name = model.family.__class__.__name__
    link_name = model.family.link.__class__.__name__
    if len(predictors) > predictors_len:
        predictors = predictors[:predictors_len] + " + ..."
    repr = f"{response_name} ~ {family_name}({predictors}, link = {link_name})"
    return repr
